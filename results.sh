for t in pgbench skewed skewed-n read-write; do

    for c in checksums no-checksums; do

        for s in 100 1000 10000; do

            if [ ! -d "$t/$c/$s" ]; then
                continue
            fi

            tps=`grep including $t/$c/$s/benchmark/pgbench.log | awk '{print $3}' | sed 's/\..*//'`

            wal_min=`head -n 1 $t/$c/$s/benchmark/xlog.csv | awk '{print $8}'`
            wal_max=`tail -n 2 $t/$c/$s/benchmark/xlog.csv | head -n 1 | awk '{print $8}'`

            echo $t $c $s $tps $((wal_max-wal_min))

        done

    done

done

