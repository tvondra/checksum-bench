#!/bin/sh

PATH=/home/tomas/pg-checksums/bin:$PATH
d=$1

for i in `seq 1 $((24*3600+60))`; do

	psql -t -c "select now(), extract(epoch from now()), pg_current_xlog_location(), pg_xlog_location_diff(pg_current_xlog_location(), '0/0');" pgbench >> $d/xlog.csv 2>&1;

	psql -t -c "select now(), extract(epoch from now()), * from pg_stat_user_tables" pgbench >> $d/stat-user-tables.csv 2>&1
	psql -t -c "select now(), extract(epoch from now()), * from pg_statio_user_tables" pgbench >> $d/statio-user-tables.csv 2>&1

	psql -t -c "select now(), extract(epoch from now()), * from pg_stat_user_indexes" pgbench >> $d/stat-user-indexes.csv 2>&1
	psql -t -c "select now(), extract(epoch from now()), * from pg_statio_user_indexes" pgbench >> $d/statio-user-indexes.csv 2>&1

	psql -t -c "select now(), extract(epoch from now()), relname, pg_relation_size(relid) from pg_stat_user_tables" pgbench >> $d/table-sizes.csv 2>&1
	psql -t -c "select now(), extract(epoch from now()), indexrelname, pg_relation_size(indexrelid) from pg_stat_user_indexes" pgbench >> $d/index-sizes.csv 2>&1

	psql -t -c "select now(), extract(epoch from now()), * from pg_stat_bgwriter" pgbench >> $d/stat-bgwriter.csv 2>&1

	psql -t -c "select now(), extract(epoch from now()), pg_database_size(datname), * from pg_stat_database" pgbench >> $d/databases.csv 2>&1

	sleep 1;

done;
